# README #

Here's a bunch of filters that I ported over to unity, I won't be using those so I'll just give them away.

You should optimize them before using any of them in release builds, each shader file also has the creator, source and other misc informations included with it, all of them have specific licenses that you have to take into account if you are going to use them.


I implemented them by using a Screengrab for ease of use so you can just drop them on a quad and put the quad in front of your camera to apply the effect or you could also add them to the material list on a renderer to apply the effect only to that model.


Texture1 in most case is noise, Kuwahara Aniso' 's k0 is 'kernel111'.


Download @ https://bitbucket.org/Eideren/npr_post_processing/downloads/